//
//  ViewController.m
//  RecipeTable
//
//  Created by Jordan Hipwell on 1/26/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *recipeData;

@end


@implementation ViewController

static NSString * RecipeTitleID = @"recipeTitle";
static NSString * RecipeImageID = @"recipeImage";
static NSString * RecipeInstructionsID = @"recipeInstructions";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //get recipe data from plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    self.recipeData = [[NSArray alloc] initWithContentsOfFile:path];
    
    CGFloat statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    //set up background
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    backgroundImageView.image = [UIImage imageNamed:@"wood-bg"];
    backgroundImageView.backgroundColor = [UIColor redColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:backgroundImageView];
    
    //set up header label
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont fontWithName:@"Zapfino" size:20];
    label.text = @"Recipes";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.65];
    [label sizeToFit];
    label.frame = CGRectMake(0, statusBarHeight, self.view.frame.size.width, label.frame.size.height);
    [self.view addSubview:label];
    
    //set up separator view
    CGFloat verticalPosition = label.frame.origin.y + label.frame.size.height;
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, verticalPosition, self.view.frame.size.width, 1)];
    separator.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    [self.view addSubview:separator];
    
    //set up table view
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, verticalPosition, self.view.frame.size.width, self.view.frame.size.height - verticalPosition)];
    tableView.backgroundColor = [UIColor clearColor];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.rowHeight = 100;
    [self.view addSubview:tableView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.recipeData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) { //haven’t created a cell with that id before
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *selectedView = [[UIView alloc] init];
    selectedView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    selectedView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
    cell.selectedBackgroundView = selectedView;
    
    //recipe image
    CGFloat padding = 8;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.separatorInset.left, padding, 100 - padding, tableView.rowHeight - padding * 2)];
    imageView.image = [UIImage imageNamed:self.recipeData[indexPath.row][RecipeImageID]];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [cell.contentView addSubview:imageView];
    
    //recipe title
    CGFloat horizontalPosition = imageView.frame.origin.x + imageView.frame.size.width + 15;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(horizontalPosition, 0, cell.frame.size.width - horizontalPosition, tableView.rowHeight)];
    label.text = self.recipeData[indexPath.row][RecipeTitleID];
    label.numberOfLines = 2;
    label.font = [UIFont fontWithName:@"Papyrus" size:[UIFont preferredFontForTextStyle:UIFontTextStyleBody].pointSize];
    label.textColor = [UIColor blackColor];
    [cell.contentView addSubview:label];
    
    return cell;
}

#pragma mark - UITableViewDelegate

//TODO: Add didSelectRowAtIndexPath to show instructions for recipe :D


@end
